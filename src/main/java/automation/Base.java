package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
    public Base(final WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    protected boolean waitVisibilityElement(final WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    protected void click(final WebElement webElement) {
        waitVisibilityElement(webElement);
        webElement.click();
    }

    protected void moveToElement(final WebElement webElement) {
        Actions action = new Actions(driver);
        waitVisibilityElement(webElement);
        action.moveToElement(webElement);
    }

    protected void setText(final WebElement webElement, final String text) {
        waitVisibilityElement(webElement);
        webElement.sendKeys(text);
    }

    protected String getText(final WebElement webElement) {
        waitVisibilityElement(webElement);
        return webElement.getText();
    }

    protected boolean waitVisibilityElement(final String xpath) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
