package automation.pageObject;

import automation.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MailPageObject extends Base {


    public MailPageObject(WebDriver driver) {
        super(driver);
    }

    //поле для ввода логина
    @FindBy(xpath = ".//input[@data-testid='login-input']")
    private WebElement inputLoginField;

    //кнопка "Ввести пароль"
    @FindBy(xpath = ".//button[@data-testid='enter-password']")
    private WebElement enterPasswordButton;


    //поле для ввода пароля
    @FindBy(xpath = ".//input[@data-testid='password-input']")
    private WebElement inputPasswordField;

    //Кнопка входа в почту
    @FindBy(xpath = ".//button[@data-testid='login-to-mail']")
    private WebElement enterToMaleButton;

    //Кнопка "Написать письмо"
    @FindBy(xpath = ".//a[@title='Написать письмо']")
    private WebElement writeMailButton;

    //Поле для написания письма
    @FindBy(xpath = ".//div[@class[contains(.,'contactsContainer')]]")
    private WebElement writeMailField;

    //Поле для ввода адреса почты
    @FindBy(xpath = ".//div[@data-type='to']//input")
    private WebElement addressMailField;

    //Поле для ввода темы письма
    @FindBy(xpath = ".//input[@name='Subject']")
    private WebElement emailSubjectField;

    //Поле для ввода текста письма
    @FindBy(xpath = ".//div[@role='textbox']")
    private WebElement emailTextField;

    //Кнопка "Отправить" письмо
    @FindBy(xpath = ".//span[@title='Отправить']")
    private WebElement sendMailButton;

    //Кнопка закрыть для окна "Письмо отправлено"
    @FindBy(xpath = ".//span[@class='button2 button2_has-ico button2_close button2_pure button2_clean button2_short button2_hover-support']")
    private WebElement mailSentCloseButton;

    //Кнопка "Входящие"
    @FindBy(xpath = ".//a[@href='/inbox/']")
    private WebElement incomingEmailsButton;

    //Кнопка "Письма себе"
    @FindBy(xpath = ".//span//span[contains(.,'Письма себе')]/parent::div")
    private WebElement emailsToYourselfButton;

    //Входящее во время теста письмо
    @FindBy(xpath = ".//span/span[contains(.,'хэллоу, галактика')]")
    private WebElement thisIncomingMail;

    //Тема письма на формe просмотра
    @FindBy(xpath = ".//h2[@class[contains(.,'thread__subject')]]")
    private WebElement thisIncomingMailSubjectHeader;

    //Текст письма на формe просмотра
    @FindBy(xpath = ".//div[contains(text(),'хэллоу, прием, Земляне!')]")
    private WebElement thisIncomingMailText;

    //Кнопка "Скачать" для прикрепленного файла
    @FindBy(xpath = ".//a[@class='attach-list__controls-element-download']")
    private WebElement downloadAttach;

    //Кнопка "Настройки"
    @FindBy(xpath = ".//span[@class[contains(., 'settings')]]")
    private WebElement settingsButton;

    //Кнопка "Все настройки"
    //  @FindBy(xpath = ".//div[@class='list-item__selected']")
    @FindBy(xpath = " .//div[contains(text(),'Все настройки')]")
    private WebElement allSettingsButton;

    //Кнопка "Имя и подпись"
    @FindBy(xpath = ".//h3[@data-test-id='card-header' and contains(.,'Имя и подпись')]")
    private WebElement nameAndSignButton;

    //Кнопка редактирования подписи
    @FindBy(xpath = ".//button[@data-test-id='edit']")
    private WebElement editSignButton;

    //Поле для редактирования подписи
    @FindBy(xpath = ".//div[@role='textbox']")
    private WebElement editSignField;

    //"Сохранить" на форме редактирования подписи
    @FindBy(xpath = ".//button[@data-test-id='save' and @type = 'submit']")
    private WebElement editSignSaveButton;

    //Кнопка "Вернуться в почту"
    @FindBy(xpath = ".//span[contains(text(), 'Вернуться в почту')]")
    private WebElement backToMaleButton;

    //Выделить все письма
    @FindBy(xpath = ".//span/span[contains(., 'Привет')]")
    private Set<WebElement> checkMalesButton;

    //Кнопка "Удалить" письма
    @FindBy(xpath = ".//span[@title='Удалить']/parent::div")
    private WebElement deleteEmailsButton;

    //Текст "Писем нет"
    @FindBy(xpath = ".//span[@class='octopus__title']")
    private WebElement noMessages;

    //Ввод логина
    public void setInputLoginField(final String text) {
        setText(inputLoginField, text);
    }

    //Нажатие "Ввести пароль"
    public void clickEnterPasswordButton() {
        click(enterPasswordButton);
    }

    //Ввод пароля
    public void setInputPasswordField(final String text) {
        setText(inputPasswordField, text);
    }

    //Вход в почту
    public void clickEnterToMale() {
        click(enterToMaleButton);
    }

    //Нажатие "написать письмо"
    public void clickWriteMailButton() {
        click(writeMailButton);
    }

    //Ожидание формы написания письма
    public void waitWriteMailField() {
        waitVisibilityElement(writeMailField);
    }

    //Ввод адреса
    public void setAddressMailField(final String text) {
        setText(addressMailField, text);
    }

    //Ввод темы
    public void setEmailSubjectField(final String text) {
        setText(emailSubjectField, text);
    }

    //Ввод текста
    public void setEmailTextField(final String text) {
        setText(emailTextField, text);
    }

    //прикрепление файла к письму
    public void inputFile() {
        String filePath = new File("src/main/resources/testtttt7777888.txt").getAbsolutePath();
        getDriver().findElement(By.xpath(".//button[@tabindex=\"500\"]/input[@type=\"file\"]")).sendKeys(filePath);
    }

    //Отправка письма
    public void clickSendMailButton() {
        //иначе письмо улетает в спам
        try {
            Thread.sleep(3500);
            click(sendMailButton);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //закрытие окна "Письмо отправлено"
    public void closeWindowMailSent() {
        if (waitVisibilityElement(mailSentCloseButton)) {
            click(mailSentCloseButton);
        }
    }

    //Нажатие на "Входящие"
    public void clickIncomingEmailsButton() {
        click(incomingEmailsButton);
    }

    //Открытие папки "Письма себе"
    public void clickEmailsToYourself() {
        click(emailsToYourselfButton);
    }

    //Проверка наличия входящего письма
    public void assertIncomingMail() {
        waitVisibilityElement(thisIncomingMail);
    }

    //Открытие входящего письма
    public void clickThisIncomingMail() {
        click(thisIncomingMail);
    }

    //Проверка темы
    public void assertMailSubjectHeader(String emailSubject) {
        waitVisibilityElement(thisIncomingMailSubjectHeader);
        thisIncomingMailSubjectHeader.equals(emailSubject);
    }

    //Проверка текста
    public void assertMailText(String emailText) {
        waitVisibilityElement(thisIncomingMailText);
        thisIncomingMailText.getText().equals(emailText);
    }

//    //проверка наличие скачанного файла в дирректории загрузки
//    public void ckeckDownloadAttach() {
//       getDriver().
//    }

    //Скачивание аттача
    public void clickDownloadAttach() {
        click(downloadAttach);
    }

    //Нажатие на "Настройки"
    public void clickSettingsButton() {
        click(settingsButton);
    }

    //Переход ко всем настройкам
    public void clickAllSettingsButton() {
        click(allSettingsButton);
    }

    //Нажатие на "Имя и подпись"
    public void clickNameAndSignButton() {
        ArrayList tabs2 = new ArrayList(getDriver().getWindowHandles());
        getDriver().switchTo().window(String.valueOf(tabs2.get(1)));
        click(nameAndSignButton);
    }

    //Нажатие на "Редактировать имя и подпись"
    public void clickEditSignButton() {
        click(editSignButton);
    }

    //Ввод новой подписи
    public void setEditSignField(final String text) {
        editSignField.clear();
        setText(editSignField, text);
    }

    //Сохранение изменений
    public void clickEditSignSaveButton() {
        click(editSignSaveButton);
    }

    //Возвращение к списку писеи
    public void clickBackToMaleButton() {
        click(backToMaleButton);
    }

    //Выбор писем
    public void clickCheckMaleButtons() {
        List<WebElement> checks1 = getDriver().findElements(By.xpath(".//span/span[contains(., 'Привет')]"));
        List<WebElement> checks = getDriver().findElements(By.xpath(".//a[@class[contains(.,'s-tooltip-direction_letter-bottom')]]//button/div/span"));
        List<WebElement> checks3 = getDriver().findElements(By.xpath(".//div[@class=\"checkbox__box checkbox__box_disabled\"]"));

        for (WebElement webElement : checks3) {
            Actions actions = new Actions(getDriver());
            actions.moveToElement(webElement).build().perform();
            click(webElement);
        }
    }

    //Нажатие на "Удалить"
    public void clickDeleteEmailsButton() {
        click(deleteEmailsButton);
    }

    //Проверка, что писем нет
    public void waitNoMessagesText() {
        waitVisibilityElement(noMessages);
    }
}


