package automation;

import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class ChromeBaseTest extends TestCaseBase {

    @Rule
    public TestWatcher watcher;

    {
        watcher = new TestWatcher() {
            @Override
            protected void starting(Description description) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                String downloadFilepath = "src/";
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", downloadFilepath);
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                driver = new ChromeDriver(options);

                //   ChromeOptions options = new ChromeOptions();
                DesiredCapabilities ds = new DesiredCapabilities();
                ds.setCapability(ChromeOptions.CAPABILITY, options);
                URL hub = null;
                try {
                    hub = new URL(ggrUrl + "/wd/hub");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                //  driver = new RemoteWebDriver(hub, ds);
                // driver = new ChromeDriver();
                // driver = new RemoteWebDriver("C:\\autom\\selenoid\\chromedriver.exe");
            }

            @Override
            protected void succeeded(Description description) {

            }

            @Override
            protected void failed(Throwable e, Description description) {

            }

            @Override
            protected void finished(Description description) {
                driver.quit();
                if (driver != null) {
                    driver.quit();
                }
            }
        };
    }
}
