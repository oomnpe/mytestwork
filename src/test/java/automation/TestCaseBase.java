package automation;

import org.openqa.selenium.WebDriver;

public class TestCaseBase {

    protected WebDriver driver;

    protected String ggrUrl = "http://localhost:4445";

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
