package automation.test;

import automation.ChromeBaseTest;
import automation.pageObject.MailPageObject;
import org.junit.Test;

public class WorkWithMailTest extends ChromeBaseTest {

    private final String login = "vosekvosekvosek";
    private final String pass = "asfasdfafaq3235";
    private String emailSubject = "хэллоу, галактика Млечный путь ))) " + Math.random();
    private String emailText = "хэллоу, прием, Земляне! )))" + Math.random();
    private String signature = "Яяяяя такойтович )))" + Math.random();

    @Test
    public void startTest() {
        getDriver().get("https://mail.ru/");
        getDriver().manage().window().maximize();
        MailPageObject ts = new MailPageObject(getDriver());

        //Ввод логина
        ts.setInputLoginField(login);

        //Клик по кнопке "Ввести пароль"
        ts.clickEnterPasswordButton();

        //Ввод пароля
        ts.setInputPasswordField(pass);

        //Клик "Войти в почту"
        ts.clickEnterToMale();

        //Клик "Написать письмо"
        ts.clickWriteMailButton();

        //Проверка, что открылась форма нписания письма
        ts.waitWriteMailField();

        //Ввод адреса емэйла
        ts.setAddressMailField(login + "@mail.ru");

        //Ввод темы письма
        ts.setEmailSubjectField(emailSubject);

        //Ввод текста письма
        ts.setEmailTextField(emailText);

        //Прикрепление файла
        ts.inputFile();

        //Клик "Отправить письмо"
        ts.clickSendMailButton();

        //Закрытие окна "Письмо отправлено"
        ts.closeWindowMailSent();

        //Клик "Входящие"
        ts.clickIncomingEmailsButton();

        //Клик "Письма себе"
        ts.clickEmailsToYourself();

        //Проверка наличия письма
        ts.assertIncomingMail();

        //Клик по входящему письму
        ts.clickThisIncomingMail();

        //Проверка темы письма
        ts.assertMailSubjectHeader(emailSubject);

        //Проверка текста письма
        ts.assertMailText(emailText);

        //Скачивание прикрепленного файла
        ts.clickDownloadAttach();

        //Нажатие на кнопку "Настройки"
        ts.clickSettingsButton();

        //Нажатие на кнопку "Все настройки"
        ts.clickAllSettingsButton();

        //Клик на "Имя и подпись"
        ts.clickNameAndSignButton();

        //Клик "Изменить подпись"
        ts.clickEditSignButton();

        //Ввод новой подписи
        ts.setEditSignField(signature);

        //Клик "Сохранить подпись"
        ts.clickEditSignSaveButton();

        //Клик "Вернуться к письмам"
        ts.clickBackToMaleButton();

        //Клик "Написать письмо"
        ts.clickWriteMailButton();

        //Ввод адреса
        ts.setAddressMailField(login + "@mail.ru");

        //Ввод темы письма
        ts.setEmailSubjectField(emailSubject);

        //Ввод текст письма
        ts.setEmailTextField(emailText);

        //Клик "Отправить письмо"
        ts.clickSendMailButton();

        //Закрытие окна "Письмо отправлено"
        ts.closeWindowMailSent();

        //Клик "Входящие"
        ts.clickIncomingEmailsButton();

        //Клик "Письма себе"
        ts.clickEmailsToYourself();

        //Проверка наличия входящего письма
        ts.assertIncomingMail();

        //Открытие входящего письма
        ts.clickThisIncomingMail();

        //Проверка темы письма
        ts.assertMailSubjectHeader(emailSubject);

        //Првоерка текста письма
        ts.assertMailText(emailText);

        //Клик "Входящие"
        ts.clickIncomingEmailsButton();

        //Клики по чекерам напротив полученных писем
        ts.clickCheckMaleButtons();

        //Удаление выбранных писем
        ts.clickDeleteEmailsButton();

        //Проверка, что писем нет
        ts.waitNoMessagesText();
    }
}
